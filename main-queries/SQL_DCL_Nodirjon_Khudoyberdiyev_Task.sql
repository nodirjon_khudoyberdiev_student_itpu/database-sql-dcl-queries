
-- Step 1: Create a new user
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
REVOKE ALL ON DATABASE dvdrental FROM rentaluser;


-- Step 2: Grant connect permission to the user
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;


-- Step 3: Grant SELECT permission for the "customer" table
GRANT SELECT ON TABLE customer TO rentaluser;

SET ROLE rentaluser;

SELECT * FROM customer; 

SELECT * FROM film; --fail

RESET ROLE;


--Create a new user group
CREATE GROUP rental;
--Add "rentaluser" to the "rental" group
GRANT rental TO rentaluser;

--Grant INSERT and UPDATE permissions for the "rental" table to the "rental" group
GRANT INSERT, UPDATE ON TABLE rental TO rental;

SET ROLE rental;

INSERT INTO "rental" ("rental_id", "rental_date", "inventory_id", "customer_id", "return_date", "staff_id")
             VALUES (90000, '2024-04-07 15:00:00', 1, 23, '2024-04-30 15:00:00', 2);
			 
UPDATE "rental"
SET "return_date" = '2024-04-24 14:00:00'
WHERE "rental_id" = 90000;

RESET ROLE;

REVOKE INSERT ON rental FROM rental;

SET ROLE rental;

INSERT INTO "rental" ("rental_id", "rental_date", "inventory_id", "customer_id", "return_date", "staff_id") --fail
             VALUES (90005, '2024-04-27 15:00:00', 1, 23, '2024-04-30 15:00:00', 2);
			 
RESET ROLE;

SELECT DISTINCT customer.customer_id, customer.first_name, customer.last_name
FROM customer
JOIN rental USING (customer_id)
JOIN payment USING (customer_id)
WHERE rental.rental_id IS NOT NULL AND payment.payment_id IS NOT NULL
limit(1);

--Create a personalized role for a customer
CREATE ROLE client_nodirjon;
GRANT USAGE ON SCHEMA public TO client_nodirjon;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE payment TO client_patricia_johnson;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE rental TO client_patricia_johnson;

--Grant SELECT permission for the "rental" and "payment" tables to the personalized role
ALTER TABLE payment ENABLE ROW LEVEL SECURITY;
ALTER TABLE rental ENABLE ROW LEVEL SECURITY;

CREATE POLICY client_data_policy ON payment USING (customer_id = 2);
CREATE POLICY client_data_policy ON rental USING (customer_id = 2);

ALTER TABLE rental FORCE ROW LEVEL SECURITY;
ALTER TABLE payment FORCE ROW LEVEL SECURITY;


SET ROLE client_nodirjon;

SELECT * FROM rental;
SELECT * FROM payment;

RESET ROLE;